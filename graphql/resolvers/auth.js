const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");

const User = require('../../models/user');

const resolvers = {
  Mutation: {
    createUser: async ( parent, args ) => {
      try {
        const existingUser = await User.findOne({ username: args.userInput.username });

        if (existingUser) {
          throw new Error("User already exists");
        }
        const hashedPassword = await bcrypt.hash(args.userInput.password, 12);

        const user = new User({
          username: args.userInput.username,
          password: hashedPassword
        });

        const result = await user.save();
        return { ...result._doc, password: null, _id: result.id };
      } catch (err) {
        throw err;
      }
    }
  },
  Query: {
    login: async ( parent, { username, password } ) => {

      const user = await User.findOne({ username: username });

      if (!user) {
        throw new Error("User doesn't exist!");
      }
      const isEqual = bcrypt.compare(password, user.password);
      if (!isEqual) {
        throw new Error("Invalid Credentials");
      }
      const token = jwt.sign(
        { userId: user.id, username: user.username },
        "somesupersecretkey",
        {
          expiresIn: "1h"
        }
      );
      return {
        userId: user.id,
        token: token,
        tokenExpiration: 1
      };
    }
  }
};


module.exports = resolvers;