const { gql } = require('apollo-server');

const typeDefs = gql`

type User {
    _id: ID!
    username: String!
    password: String
}

type AuthData {
    userId: ID!
    token: String!
    tokenExpiration: Int!
}

input UserInput {
    username: String!
    password: String!
}

type Query {
    login(username: String!, password: String!): AuthData!
}

type Mutation  {
    createUser(userInput: UserInput): User
}
`;

module.exports = typeDefs;