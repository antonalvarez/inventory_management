import React, { Component } from "react";
import { toast } from "react-toastify";


import "./Login.scss";
import authContext from "../../context/auth-context";
import { GRAPH_URL } from "../../config/constants";

import company_logo from "../../assets/img/ui/apollo_logo.png";
import facebook_logo from "../../assets/img/ui/fb.png";
import aesi_logo from "../../assets/img/ui/aesi.png";
import support_logo from "../../assets/img/ui/support.png";
import warehouse_logo from "../../assets/img/ui/warehouselogo.png";
import user_logo from "../../assets/img/ui/user.png";
import padlock_logo from "../../assets/img/ui/padlock.png";

class LoginPage extends Component {
  state = {
    isLogin: true
  };

  static contextType = authContext;

  constructor(props) {
    super(props);
    this.usernameEl = React.createRef();
    this.passwordEl = React.createRef();
  }

  switchModeHandler = () => {
    this.setState(prevState => {
      return { isLogin: !prevState.isLogin };
    });
  };

  submitHandler = event => {
    event.preventDefault();
    const username = this.usernameEl.current.value;
    const password = this.passwordEl.current.value;

    if (username.trim().length === 0 || password.trim().lenght === 0) {
      return;
    }

    let requestBody = {
      query: `
            query Login($username: String!, $password: String!){
              login(username: $username, password: $password){
                userId
                token
                tokenExpiration
              }
            }
          `,
      variables: {
        username: username,
        password: password
      }
    };

    if (!this.state.isLogin) {
      requestBody = {
        query: `
              mutation CreateUser($username: String!, $password: String!){
                createUser(userInput: {username: $username, password: $password}){
                  _id
                  username
                }
              }
            `,
        variables: {
          username: username,
          password: password
        }
      };
    }

    fetch(GRAPH_URL, {
      method: "POST",
      body: JSON.stringify(requestBody),
      headers: {
        "Content-Type": "application/json"
      }
    })
      .then(res => {
        if (res.status !== 200 && res.status !== 201) {
          throw new Error("Failed");
        }
        return res.json();
      })
      .then(resData => {
        if (resData.data) {
          this.context.login(
            resData.data.login.token,
            resData.data.login.userId,
            resData.data.login.tokenExpiration
          );
        }else{
          toast.error(resData.errors[0].message, {
            position: "top-right",
            autoClose: false,
            hideProgressBar: false,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: true
          });
        }
      })
      .catch(err => {
        console.log(err);        
      });
  };

  render() {
    return (
      <div className="row">
        <div className="logo-left">
          <img src={company_logo} alt="logo" />
        </div>
        <div className="form-right content-center">
          <div className="form-bg-green" />
          <div className="form-container">
            <form className="auth-form" onSubmit={this.submitHandler}>
              <div className="form-content">
                <div className="module-container">
                  <img src={warehouse_logo} alt="logo" />
                </div>
                <div className="logo-title">
                  <span>
                    <b>INVENTORY MNGT.</b>
                  </span>
                </div>

                <div className="error-message"></div>

                <div className="login-input">
                  <div className="row">
                    <input
                      type="text"
                      id="username"
                      label="User Name"
                      ref={this.usernameEl}
                    />
                    <img src={user_logo} alt="user" />
                  </div>

                  <div className="row margin-top-20">
                    <input
                      type="password"
                      id="password"
                      ref={this.passwordEl}
                    />
                    <img src={padlock_logo} alt="padlock" />
                  </div>

                  <div className="row forgot-pass-link margin-top-10">
                    <button type="button" onClick={this.switchModeHandler}>
                     {this.state.isLogin ? "Create Account ?" : "Login?"}
                   </button>
                  </div>

                  <div className="row margin-top-20">
                    <button type="submit" className="button-login" id="btn-submit">
                      {this.state.isLogin ? "Login" : "Sign up"}
                    </button>
                  </div>
                </div>
              </div>
            </form>
          </div>
        </div>
        <div className="login-links">
          <div className="row">
            <img src={facebook_logo} alt="facebook" />
            <img src={aesi_logo} alt="aesi" />
            <img src={support_logo} alt="support" />
          </div>
        </div>
      </div>
    );
  }
}

export default LoginPage;
