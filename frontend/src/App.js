import React, { Component } from "react";
import { BrowserRouter, Route, Redirect, Switch } from "react-router-dom";
import { ToastContainer } from "react-toastify";

// insert module pages here
import LoginPage from "./pages/Login/Login";
import DashboardPage from "./pages/Dashboard/Dashboard";

// insert utilities components here
import MainNavigation from "./components/contents/MainNavigation/MainNavigation";
import AuthContext from "./context/auth-context";

import "./App.css";
import "react-toastify/dist/ReactToastify.css";

class App extends Component {
  state = {
    token: null,
    userId: null
  };

  login = (token, userId, tokenExpiration) => {
    this.setState({ token: token, userId: userId });
  };

  logout = (token, userId, tokenExpiration) => {
    this.setState({ token: null, userId: null });
  };

  render() {
    return (
      <BrowserRouter>
        <React.Fragment>
          <AuthContext.Provider
            value={{
              token: this.state.token,
              userId: this.state.userId,
              login: this.login,
              logout: this.logout
            }}
          >
            <ToastContainer
              position="top-right"
              autoClose={5000}
              hideProgressBar
              newestOnTop={false}
              closeOnClick={false}
              rtl={false}
              pauseOnVisibilityChange
              draggable
              pauseOnHover
            />
            {this.state.token && <MainNavigation />}

            <main className="main-content">
              <Switch>
                {this.state.token && (
                  <Redirect from="/" to="/dashboard" exact />
                )}
                {this.state.token && (
                  <Redirect from="/login" to="/dashboard" exact />
                )}
                {!this.state.token && (
                  <Route path="/login" component={LoginPage} />
                )}

                {/* add module pages here */}
                {this.state.token && (
                  <Route path="/dashboard" component={DashboardPage} />
                )}
                {!this.state.token && <Redirect to="/login" exact />}
              </Switch>
            </main>
          </AuthContext.Provider>
        </React.Fragment>
      </BrowserRouter>
    );
  }
}
export default App;
