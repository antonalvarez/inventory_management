import React from 'react';
import ReactDOM from 'react-dom';

// add global css files
import '@fortawesome/fontawesome-free/css/all.min.css';
import 'bootstrap-css-only/css/bootstrap.min.css';
import "./assets/scss/mdb.scss";
import "./assets/scss/global/margin.scss";

import App from './App';

ReactDOM.render(<App />, document.getElementById('root'));
